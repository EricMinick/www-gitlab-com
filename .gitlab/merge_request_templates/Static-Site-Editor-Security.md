<!-- Template for fixing a security vulnerability -->

## What does this MR do?

<!-- Briefly describe what this MR is about -->


## Related issues

<!-- Mention the issue(s) this MR closes or is related to -->

Closes


/label ~"group::static site editor" ~security
