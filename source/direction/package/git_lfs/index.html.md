---
layout: markdown_page
title: "Category Direction - Git LFS"
description: "Git LFS (Large File Storage) is a Git extension that reduces the impact of large files in your repository by downloading the relevant versions of them lazily."
canonical_path: "/direction/package/git_lfs/"
---

- TOC
{:toc}

## Git LFS

Git LFS (Large File Storage) is a Git extension, which reduces the impact of large files in your repository by downloading the relevant versions of them lazily. Specifically, large files are downloaded during the checkout process rather than during cloning or fetching..

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AGit%20LFS)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)
- [Documentation](https://docs.gitlab.com/ee/administration/lfs/manage_large_binaries_with_git_lfs.html)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com)), however the prioritization, design and implementation of features and bugs is owned by the [Create:Source Code Group](/direction/create/source_code_management/).

## Use cases

1. Version large files—even those as large as a couple GB in size—with Git.
1. Automatically detect LFS-tracked files and clone them via HTTP
1. Download less data. This means faster cloning and fetching from repositories that deal with large files.
1. Host more in your Git repositories. External file storage makes it easy to keep your repository at a manageable size.

## What's next & why

Up next is [gitlab-#15079](https://gitlab.com/gitlab-org/gitlab/-/issues/15079) which will ensure that the Gitlab archiver honors Git-LFS references in a repository when it's streaming an archive to a user. And [gitlab-#218666](https://gitlab.com/gitlab-org/gitlab/-/issues/218666) resolves an issue where the LFS label is absent on files with long name/path.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is "Viable" (see our [definitions of maturity levels](/direction/maturity/)).

For a list of key deliverables and expected outcomes, check out the epic, [Make Git LFS Loveable](https://gitlab.com/groups/gitlab-org/-/epics/302), which includes links and expected timing for each issue.

## Competitive landscape

* [Atlassian](https://www.atlassian.com/git/tutorials/git-lfs)
* [GitHub](https://github.com/)
* [JFrog](https://jfrog.com/) 

## Top user issue(s)
The top user issue is [gitlab-#15079](https://gitlab.com/gitlab-org/gitlab/issues/15079), which aims to resolve a problem in which the Gitlab archiver doesn't honor Git-LFS references in a repository when it's streaming an archive to a user.

## Top internal customer issue(s)
There are no internal issues at the moment. 

## Top Vision Item(s)
The top vision item is [gitlab-#14114](https://gitlab.com/gitlab-org/gitlab/issues/14114), which will add the ability to set limits and quotas for Git LFS. 
