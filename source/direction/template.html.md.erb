---
layout: markdown_page
title: "GitLab Direction"
description: "Our vision is to replace disparate DevOps toolchains with a single application that is pre-configured to work by default across the entire DevOps lifecycle."
canonical_path: "/direction/"
extra_css:
  - maturity.css
  - direction.css
---

## On this page
{:.no_toc}

- TOC
{:toc}

This page introduces the GitLab product vision, where we're headed over the next few years, and our plan to deliver over the next year.

## Vision

Our vision is to replace DevOps toolchains with a
[single application](/handbook/product/single-application/) that is
pre-configured to work by default across the entire DevOps lifecycle.

Many organizations are in the midst of evolving from classic development
paradigms, to DevOps. They want faster cycle time, higher quality outcomes, and
less risk. Every organization, large or small, deserves to operate at peak
efficiency. GitLab’s job is to help those companies along, accelerate their
evolution, and provide optimized tooling once they get there. We do this by
leveraging the best practices of 100,000 organizations co-developing the DevOps
platform of their dreams. We deliver
[Seed then nuture](/company/strategy/#seed-then-nurture), and
[mature product surface area over time](/direction/maturity/), all the while,
focusing on customer results. We’re informed by data, and ROI-driven; balancing
that with fleshing out the breadth of our vision as quickly as possible so
people everywhere understand where we’re going, what’s possible, and how they
can contribute. We believe in the emergent benefits of a
[single application](/handbook/product/single-application/) for the entire
DevOps lifecycle.

You can read more about the principles that guide our prioritization process in
our [product handbook](/handbook/product/product-principles/). You can also read
our [GitLab as a Product](/handbook/product/#gitlab-as-a-product) section which
describes the principles that are used to guide GitLab itself forward.

[Product Vision, Strategy, and 2020 Plan slides](https://docs.google.com/presentation/d/11fKMOWl6pVx-BCc6LXrHXfvRXZWU3USrEHSFWgpblL4/edit?usp=sharing)

### Trend Towards Consolidation

Continuing apace after Microsoft's 2018 [acquisition of GitHub](https://blogs.microsoft.com/blog/2018/10/26/microsoft-completes-github-acquisition/), the trend to consolidate DevOps companies seems here to stay. In January 2019, [Travis CI was acquired by Idera](https://techcrunch.com/2019/01/23/idera-acquires-travis-ci/), and in February 2019 we saw [Shippable acquired by JFrog](https://techcrunch.com/2019/02/21/jfrog-acquires-shippable-adding-continuous-integration-and-delivery-to-its-devops-platform/). Atlassian and GitHub now both bundle CI/CD with SCM, alongside their ever-growing related suite of products. In January 2020, [CollabNet acquired XebiaLabs to build out their version of a comprehensive DevOps solution](https://xebialabs.com/company/press/collabnet-versionone-and-xebialabs-combine-to-create-integrated-agile-devops-platform/).

It's natural for technology markets go through stages as they mature: when a young technology is first becoming popular, there is an explosion of tools to support it. New technologies have rough edges that make them difficult to use, and early tools tend to center around adoption of the new paradigm. Once the technology matures, consolidation is a natural part of the life cycle. GitLab is in a fantastic position to be ahead of the curve on consolidation, but it's a position we need to actively defend as competitors start to bring more legitimately integrated products to market.

### DevOps Stages

<%= devops_diagram(["All"]) %>

<!-- ![Product Categories](categories.png) -->

DevOps is a broad space with a lot of complexity. To manage this within GitLab,
we break down the [DevOps lifecycle](https://en.wikipedia.org/wiki/DevOps_toolchain)
into a few different [sections](/handbook/product/product-categories/#hierarchy), each with its own direction page you can review.

<% data.sections.each do |sectionKey, section| %>
  <% line = "* **[#{section.name} Section Direction](#{section.direction})** - Includes the " %>
  <% stages = data.stages.stages.select{|stageKey, stage| stage.section==sectionKey} %>
  <% stageLinks = stages.map { |stageKey,stage| "[#{stage.display_name}](#{stage.direction})" } %>
  <% line += stageLinks.to_sentence + " " + "stage".pluralize(stages.count) %>
  <% if stages.count == 1 then %>
    <% stages.each do |stageKey, stage| %>
      <% groups = stage.groups.map { |groupKey, group| "[#{group.name}](#{group.direction})" } %>
      <% line += ", which includes the " + groups.to_sentence + ' ' + "group".pluralize(groups.count) %>
    <% end %>
  <% end %>
<%= line %>
<% end %>

We are investing [in the following manner](/handbook/product/product-processes/#rd-investment-allocation) across each stage.

## 3-year Strategy

### Situation
GitLab competes in a large market space, with a [TAM](/handbook/sales/tam/) estimated at $14B in 2019, rising to $71B in 2025 as we better serve additional personas and use cases. GitLab has impressive revenue growth, recently surpassing the $100M ARR milestone, with unusually high revenue growth and retention rates.  GitLab is uniquely positioned in the market with a vision to offer a single application for the entire DevOps lifecycle.  GitLab competes across numerous market segments and aims to deliver value in [80+ market categories](/direction/maturity/#category-maturity). GitLab’s product vision is uniquely ambitious, as we were the first DevOps player to take a [single application approach](/single-application/). From idea to production, GitLab helps teams improve cycle time from weeks to minutes, reduce development process costs, and enable a faster time to market while increasing developer productivity. With software “eating the world”, this is widely viewed as a mission-critical value proposition for customers.  We also have a number of [tailwinds](/handbook/leadership/biggest-tailwinds/) in the form of cloud adoption, Kubernetes adoption, and DevOps tool consolidation, which are helping fuel our rapid growth.  Finally, GitLab has an open source community and distribution model, which has exposed the value of GitLab to millions of developers, and has sped up the maturation of our product through more than 200 monthly improvements to the GitLab code base from our users.

### Strategic Challenges
1. **Tension between Breadth and Depth:** GitLab’s ambitious single application product vision means we need to build out feature function value across a very large surface area.  Our challenge is to drive the right balance between breadth and depth in our product experience.  In recent years, we have optimized for breadth. To win and retain more sophisticated enterprise customers, we need to effectively [seed then nuture](/strategy/#seed-then-nurture) in areas of the product that generate usage and revenue.  With so much product surface area to deliver in a single application experience, it is a big UX challenge to keep the experience simple, consistent, and seamless between DevOps phases.  
1. **GitLab.com and Self-Managed:** Another challenge we face is the balance between our self-managed and GitLab.com offerings.  GitLab's early paying customers were more interested in self-managed and the majority of our customers use this offering today.  As a result, we focused heavily on delivering a great self-managed customer experience.  However, as the market shifts toward cloud adoption, we are seeing an increasing demand for our GitLab.com offering.  We now need to rapidly meet the same enterprise-grade security, reliability, and performance expectations our paying customers have come to expect from self-managed in our SaaS (.com offering).  
1. **Wide Customer Profile:** We also serve a wide range of customers, from individual contributor developers to large enterprises, across all vertical markets.  This range of deployment options and customer sizes makes our business complex, and makes it hard to optimize the customer experience for all customer sizes.  The past few years we have prioritized enabling our direct sales channel, but in the process have not focused enough on great customer experiences around self-service purchase workflows, onboarding, and cross-stage adoption.  
1. **Competition:** Finally, we have formidable competition from much larger companies, including Microsoft, Atlassian, and Synopsys, to name a few.  Microsoft is starting to mimic our single application positioning, and while behind us in the journey, have substantial resources to dedicate to competing with GitLab.

### Strategic Response
1. **Focus on increasing Total Monthly Active Users [(TMAU)](/handbook/product/performance-indicators/#total-monthly-active-users-tmau):** There is a strong correlation between the number of stages customers use and their propensity to upgrade to a paid package.  In fact, **adding a stage triples conversion!**  Each product group should be laser focused on driving adoption and regular usage of their respective stages, as it should lead to higher [IACV](/handbook/sales/#incremental-annual-contract-value-iacv), reduced churn, and higher customer satisfaction.  See [this graph in Sisense, which shows the correlation increasing Stages per Namespace has with paid conversion](https://app.periscopedata.com/app/gitlab/608522/Conversion-Dashboard?widget=7985190&udv=0).
    <embed width="100%" height="400" src="<%= signed_periscope_url(chart: 7985190, dashboard: 608522, embed: 'v0') %>">
    As outlined in this [user journey](/handbook/product/growth/#adoption-journey), the most important additional stages for customers to adopt are Create to Verify, and Verify to Release, as each of these adoption steps open up three additional stages to users.  Our [goal](/company/strategy/#sequence) is to have 100M TMAU by the end of 2023.   
1. **Deliver cross-stage value:**  GitLab’s primary point of differentiation is our single application approach.  As we continue to drive value in any given stage or category, our first instinct should be to connect that feature or product experience to other parts of the GitLab product.  These cross-stage connections will drive differentiated customer value, and will be impossible for point product competitors to imitate.  Recognizing this opportunity, we have grown our R&D organization significantly over the past two years, and plan to invest an outsized amount on R&D for the next 2-3 years to extend our lead in executing against the single application product vision.  
1. **Leverage open source to rapidly achieve multi-stage product adoption.**  Our vision is to deliver a single application for the entire DevOps lifecycle.  To achieve this ambitious vision more quickly, we will leverage our powerful open source community.  Each stage should have a clear strategy for tiering the value of the stage.  When stages are early in maturity, we will bias toward including as much functionality in our Core open source version as possible, to drive more rapid adoption and greater community contributions, which will help us mature new stages faster.  Once stage adoption is achieved, we can then layer on additional value in paid tiers to encourage upgrades.
1. **Shift to depth in core product areas:**  We want to ensure the core product usage experience is great, which will lead to more paying customers and improved customer retention.  We intend to maintain our market-leading depth in stages with lovable categories, which currently are Verify (Continuous Integration) and Create (Source Code Management and Code Review).  Beyond that, we will endeavor to rapidly mature our offering to lovable in Plan (3rd most used stage), Release (4th most used stage), and Secure (important element of our Ultimate/Gold tier).  Our [goal](/company/strategy/#sequence) is to have 50% of our categories at lovable maturity by the end of 2023.
1. **Optimize for SaaS and self-service:**  We have millions of overall users, hundreds of thousands of paying users, and tens of thousands of paying organizations.  Given this volume of adoption, it’s essential that we assume users will need to serve themselves to buy & use GitLab.  This will enable us to shift a higher percentage of our customer base to a lower cost self-service buying channel, reducing sales & support costs, and improving our customer acquisition costs.  It should also lead to higher customer satisfaction overall, as assuming customers will buy & use GitLab without assistance from sales & support will force us to keep our UX quality bar very high.  We should also assume that over time a majority of our customers will prefer a SaaS delivery model, so our SaaS offering needs to have enterprise-grade security, availability, and performance. We must also ensure feature parity between self-managed and GitLab.com and that customers have an easy migration path from self-managed to GitLab.com.

## Personas

[Personas](/handbook/marketing/strategic-marketing/roles-personas/) are the people we design for. We’ve started down the path of having developers, security professionals, and operations professionals as first-class citizens; letting each person have a unique experience tailored to their needs. We want GitLab to be the main interface for all of these people. Show up at work, start your day, and load up GitLab. And that’s already happening.

But there are a ton of people involved with the development and delivery of software. That is the ultimate GitLab goal - where everyone involved with software development and delivery uses a single application so they are on the same page with the rest of their team. We are rapidly expanding our user experience for [Designers](/handbook/marketing/strategic-marketing/roles-personas/#presley-product-designer), [Compliance Managers](/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager), [Product Managers](/handbook/marketing/strategic-marketing/roles-personas/#parker-product-manager), and [Release Managers](/handbook/marketing/strategic-marketing/roles-personas/#rachel-release-manager). We’ll also be expanding to the business side, with Executive visibility and reporting. While we’re still calling it DevOps, we’re really expanding the definition of DevOps, and delivering it all as a single application.

## 1-year Plan

For 2020, we have 3 key product themes we are focused on:

1. Enterprise Readiness:  Make it easy for large enterprise customers to rapidly adopt and get value from GitLab. Relevant product direction themes include:
* [Enterprise ready GitLab management features](/direction/dev/#manage)
* [High availability Git and large repo support](/direction/dev/#create)
* [GitLab is easy to deploy and operate](/direction/enablement/#gitlab-is-easy-to-deploy-and-operate)
* [Consistently great user experience regardless of location or scale](/direction/enablement/#consistently-great-user-experience-regardless-of-location-or-scale)
* [Achieve enterprise compliance needs](/direction/enablement/#achieve-enterprise-compliance-needs)
1. Double down on strengths:  Ensure our core product experience around Source Code Management and Continuous Integration remains best in class.  Relevant product direction themes include:
* [Enhanced code review and realtime merge requests](/direction/dev/#create)
* [Git availability and performance and large repo support](/direction/dev/#create)
* [Speedy, reliable pipelines](/direction/ops/#speedy-reliable-pipelines)
* [Multi-platform support](/direction/ops/#multi-platform-support)
* [Single application CI/CD](/direction/ops/#single-application-cicd)
* [Do powerful things easily](/direction/ops/#do-powerful-things-easily)
1. World-Class Security for DevSecOps:  Deliver fully integrated security experience, allowing customers to adapt security testing and processes to developers (and not the other way around).  Relevant product direction themes include:
* [Security is a team effort](/direction/secure/#security-is-a-team-effort)
* [Shift left.  No, more left than that](/direction/secure/#shift-left-no-more-left-than-that)
* [Shift right.  Yes, right into operations](/direction/secure/#shift-right-yes-right-right-into-operations)
* [Provide actionable intelligence to ensure data-driven decisions](/direction/secure/#provide-active-intelligence-to-enable-data-driven-decisions)
* [Bring your own security tools](/direction/secure/#byot---bring-your-own-security-tools)

## Maturity

As we add new categories and stages to GitLab, some areas of the product will be deeper and more mature than others. We publish a
list of the categories, what we think their maturity levels are, and our plans to improve on our [maturity page](/direction/maturity/).

## Scope

We try to prevent maintaining functionality that is language or platform
specific because they slow down our ability to get results. Examples of how we
handle it instead are:

1. We don't make native mobile clients, we make sure our mobile web pages are great.
1. We don't make native clients for desktop operating systems, people can use [Tower](https://www.git-tower.com/) and for example GitLab was the first to have merge conflict resolution in our web applications.
1. For language translations we [rely on the wider community](https://docs.gitlab.com/ee/development/i18n/translation.html).
1. For Static Application Security Testing we rely on [open source security scanners](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks).
1. For code navigation we're hesitant to introduce navigation improvements that only work for a subset of languages.
1. For [code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality_diff.html) we reuse Codeclimate Engines.
1. For building and testing with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) we use Heroku Buildpacks.

Outside our scope are Kubernetes and everything it depends on:

1. **Network** (fabric) [Flannel](https://github.com/coreos/flannel/), Openflow, VMware NSX, Cisco ACI
1. **Proxy** (layer 7) [Envoy](https://envoyproxy.github.io/), [nginx](https://nginx.org/en/), [HAProxy](http://www.haproxy.org/), [traefik](https://traefik.io/)
1. **Ingress** [(north/south)](https://networkengineering.stackexchange.com/a/18877) [Contour](https://github.com/heptio/contour), [Ambassador](https://www.getambassador.io/),
1. **Service mesh** [(east/west)](https://networkengineering.stackexchange.com/a/18877) [Istio](https://istio.io/), [Linkerd](https://linkerd.io/)
1. **Container Scheduler** we mainly focus on Kubernetes, other container schedulers are: CloudFoundry, OpenStack, OpenShift, Mesos DCOS, Docker Swarm, Atlas/Terraform, [Nomad](https://nomadproject.io/), [Deis](http://deis.io/), [Convox](http://www.convox.com/), [Flynn](https://flynn.io/), [Tutum](https://www.tutum.co/), [GiantSwarm](https://giantswarm.io/), [Rancher](https://github.com/rancher/rancher/blob/master/README.md)
1. **Package manager** [Helm](https://github.com/kubernetes/helm), [ksonnet](http://ksonnet.heptio.com/)
1. **Operating System** Ubuntu, CentOS, [RHEL](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux), [CoreOS](https://coreos.com/), [Alpine Linux](https://alpinelinux.org/about/)

During a presentation of Kubernetes Brendan Burns talks about the 4 Ops layers
at [the 2:00 mark](https://youtu.be/WwBdNXt6wO4?t=120):

1. Application Ops
1. Cluster Ops
1. Kernel/OS Ops
1. Hardware Ops

GitLab helps you mainly with application ops. And where needed we also allow you
to monitor clusters and link them to application environments. But we intend to
use vanilla Kubernetes instead of something specific to GitLab.

Also outside our scope are products that are not specific to developing,
securing, or operating applications and digital products.

1. Identity management: Okta and Duo, you use this mainly with SaaS applications
   you don't develop, secure, or operate.
1. SaaS integration: Zapier and IFTTT
1. Ecommerce: Shopify

In scope are things that are not mainly for SaaS applications:

1. Network security, since it overlaps with application security to some extent.
1. Security information and event management (SIEM), since that measures
   applications and network.
1. Office productivity applications, since
   ["We believe that all digital products should be open to contributions, from legal documents to movie scripts and from websites to chip designs"](/company/strategy/#why)

## Quarterly Objectives and Key Results (OKRs)

To make sure our goals are clearly defined and aligned throughout the
organization, we make use of OKR's (Objectives and Key Results). Our [quarterly Objectives and Key Results](/company/okrs/)
are publicly viewable.

## Your contributions

GitLab's direction is determined by GitLab the company, and the code that is
sent by our [contributors](http://contributors.gitlab.com/). We continually
merge code to be released in the next version. Contributing is the best way to
get a feature you want included.

On [our issue tracker for CE][ce-issues] and [EE][ee-issues], many requests are
made for features and changes to GitLab. Issues with the [Accepting Merge
Requests] label are pre-approved as something we're willing to add to GitLab. Of
course, before any code is merged it still has to meet our [contribution
acceptance criteria].

[ce-issues]: https://gitlab.com/gitlab-org/gitlab-ce/issues
[ee-issues]: https://gitlab.com/gitlab-org/gitlab-ee/issues
[Accepting merge requests]: https://gitlab.com/gitlab-org/gitlab-ce/issues?state=opened&label_name=Accepting+merge+requests
[contribution acceptance criteria]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria

## How we plan releases

At GitLab, we strive to be [ambitious](/handbook/product/product-principles/#how-this-impacts-planning),
maintain a strong sense of urgency, and set aspirational targets with every
release. The direction items we highlight in our [kickoff](/handbook/product/product-processes/#kickoff-meetings)
are a reflection of this ambitious planning. When it comes to execution we aim
for
[velocity over predictability](/handbook/engineering/#velocity-over-predictability).
This way we optimize our planning time to focus on the top of the queue and
deliver things fast. We schedule 100% of what we can accomplish based on past [throughput](/handbook/engineering/management/throughput/)
and availability factors (vacation, contribute, etc.).

See our [product handbook on how we prioritize](/handbook/product/product-processes/#how-we-prioritize-work).

## Previous releases

On our [releases page](/releases/) you can find an overview of the most
important features of recent releases and links to the blog posts for each
release.

## Upcoming releases

GitLab releases a new version [every single month on the 22nd](/blog/2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab). You can find the
major planned features for upcoming releases on our [upcoming releases page](/upcoming-releases/) or see the [upcoming features for paid tiers](/direction/paid_tiers).

Note that we often move things around, do things that are not listed, and cancel
things that _are_ listed.

## Mobile strategy

Developing and delivering mobile apps with GitLab is a critical capability. Many technology companies are now managing a fleet of mobile applications, and being able to effectively build, package, test, and deploy this code in an efficient, effective way is a competitive advantage that cannot be understated. GitLab is taking improvements in this area seriously, with a unified vision across several of our DevOps stages.

### Mobile focus areas

There are several stages involved in delivering a comprehensive, quality mobile development experience at GitLab. These include, but are not necessarily limited to the following:

Manage: Comprehensive templates to get started quickly.
Create: Web IDE features that allow you to easily manage the kinds of code and artifacts you work with during mobile development.
Verify: Runners for macOS, Linux-based builds for iOS.
Package: Build archives for mobile applications.
Release: Review Apps for mobile development, code signing and publishing workflows to TestFlight or other distribution models.
Secure: Security scanning built directly into CI/CD pipelines supporting a variety of mobile coding languages.

### Mobile direction

There are a few important issues you can check out to see where we're headed. We are collecting these in [gitlab-org&769](https://gitlab.com/groups/gitlab-org/-/epics/769).

## ML/AI at GitLab

Machine learning (ML) through neural networks is a really great tool to solve hard to define, dynamic problems.
Right now, GitLab doesn't use any machine learning technologies, but we expect to use them [in the near future](https://gitlab.com/groups/gitlab-org/-/epics/2560)
for several types of problems.

#### ML/AI workflows

Using technology like [Kubeflow](https://www.kubeflow.org/) or [CML](https://cml.dev/).

#### Signal / noise separation

Signal detection is very hard in a noisy environment. GitLab intends to use
ML to warn users of any signals that stand out against the background noise in several features:

- security scans, notifying the user of stand-out warnings or changes
- error rates and log output, allowing you to rollback / automatically rollback a change if the network notices aberrant behavior


### Recommendation engines

Automatically categorizing and labelling is risky. Modern models tend to overfit, e.g. resulting
in issues with too many labels. However, similar models can be used very well in combination
with human interaction in the form of recommendation engines.

- [suggest labels to add to an issue / MR (one click to add)](https://gitlab.com/tromika/gitlab-issues-label-classification)
- suggest a comment based on your behavior
- suggesting approvers for particular code

### Smart behavior

Because of their great ability to recognize patterns, neural networks are an excellent
tool to help with scaling, and anticipating needs. In GitLab, we can imagine:

- auto scaling applications / CI based on past load performance
- prioritizing parallelized builds based on the content of a change

### Code quality

Similar to [DeepScan](https://deepscan.io/home/).

### Code navigation

Similar to [Sourcegraph](https://about.sourcegraph.com/) although we now have [code intelligence](https://docs.gitlab.com/ee/user/project/code_intelligence.html) in GitLab.

### Audit events
Identifying anomalous activity within audit events systems can be both challenging and valuable. This identification is difficult because audit events are raw, objective data points and must be interpreted against an organization's company policies. Knowing about anomalous behavior is valuable because it can allow GitLab administrators and group owners to proactively manage undesireable events.

This a difficult [problem to solve](https://about.gitlab.com/direction/manage/audit-events/#problems-to-solve), but can help to drastically reduce the overhead of managing risk within a GitLab environment.


<%= partial("direction/singlegroup", :locals => { :label => "SinglePersonGroups" }) %>

