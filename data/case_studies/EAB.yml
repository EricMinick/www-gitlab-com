title: EAB
cover_image: '/images/blogimages/eab_case_study_Image.jpg'
cover_title: |
  Increasing development speed to keep students in school
cover_description: |
  Empowering development teams with quality pipelines and collaboration by removing toolchain complexity and overhead
canonical_path: "/customers/EAB/"
twitter_image: '/images/blogimages/eab_case_study_Image.jpg'

customer_logo: '/images/case_study_logos/eab_logo.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Education
customer_location: Washington D.C., Richmond VA, Minneapolis MN, Birmingham AL
customer_solution: GitLab Premium
customer_employees: 1,300
customer_overview: |
   EAB harnesses the collective power of 1,500+ schools, colleges, and universities to uncover proven
   solutions and transformative insights to keep students in school.
customer_challenge: |
   EAB developers were looking for quality builds for their flagship product and have them be consistently green.
   They were looking to improve the speed of their builds without breaking the bank.

key_benefits:
  - |
    Consistently green build pipelines due to new testing structure
  - |
    Reduced toolchain complexity enabling developers to easily contribute across projects
  - |
    Achieved lightweight maintenance overhead

customer_stats:
  - stat:  20
    label: mins builds from 6 hours
  - stat: 888
    label: projects across 80 groups
  - stat: 18,000
    label: builds

customer_study_content:
  - title: the customer
    subtitle: Using analytics to help students graduate
    content:
      - |
       EAB works with over 1,400 colleges, universities, community colleges, K-12 districts,
       independent schools, and graduate programs in the country to help keep students enrolled in school.
      - |
       The company’s flagship Student Success Management System, “Navigate” helps identify at-risk students
       and allows staff to coordinate a plan for their success. It also helps identify students that wouldn’t
       traditionally be thought of as at-risk. These students often underperform in their major by avoiding
       specific classes or by putting their graduation date in jeopardy.



  - title: the challenge
    subtitle: Overcoming toolchain complexity
    content:
      - |
        When EAB split from its parent company, they lost the licenses of their existing tools — giving them a chance to build a clean slate.
        Development teams consolidated tools to simplify their build process. Continuous Integration processes before the split required Bamboo,
        Jenkins, Solano, and Circle all running at different times and with each team running their own. This situation created unnecessary
        complexity and added time, it would take six or eight hours to run the build a single-thread one at a time.
      - |
        “Our build was just red. Like all the time red. Because the feedback loop was so long and the velocity at which people were trying to get stuff
        merged in was so high, that they basically ignored the build,” Eliza Brock Marcum, CI Lead, EAB.



  - title: the solution
    subtitle: Auto-scaling to achieve faster pipelines
    content:
      - |
        EAB was looking for a solution for source control and CI that offered very lightweight maintenance overhead. They evaluated GitLab and decided that the monthly
        release cycle was extremely beneficial and provided the company with the future they were looking for.
      - |
        “GitLab from my perspective was vastly outpacing the competition in terms of feature releases. GitLab was later to the SCM game in comparison to GitHub or
        BitBucket, for instance, but with the rate of features being released it was very clear that you guys were gaining so much ground compared to the competition,” Brendan Mannix,
        Vice President, Engineering, EAB. “It seems like over the long run that GitLab is going to be the right choice for EAB.”
      - |
        EAB is using on-premises GitLab CI and auto-scaling, which allows the company to have pipeline builds as fast as they want for an affordable price. The perception of the developers,
        of the quality of the product, has been changing consistently since the introduction of GitLab, and the CI runs passing green on a consistent basis.
      - |
        Developers are showing energy and enthusiasm when builds pass and are adding useful tests to individual contributions. A year ago, tests were not typically passing. Now that the builds
        are serving a purpose, people are reviewing them and collaborating more. The general skill level and enthusiasm for testing has improved.

  - blockquote: I really enjoy GitLab. I like the integration. It was easy to set up and it's not too complex or complicated. I could tell you that the most common stickers on our laptop in our offices are GitLab stickers, so all of our engineers like it.
    attribution: Brendan Mannix
    attribution_title: Vice President, Engineering

  - title: the results
    subtitle: Building a collaborative environment
    content:
      - |
        In large part because of the move to GitLab, EAB was able to get into an appropriate flow. The company now merges approved
        release branches into master branches by and everything is tested to make sure it is green. There are assigned approvers for merge
        requests and the company has controls in place on code merges.

  - blockquote: We didn't have the right tools to support a quality first culture in our development group when all of our SCM and CI systems were distributed amongst different groups. Fast forward to now, you know, we are not perfect yet of course.  But with GitLab being supported throughout EAB and adopted across our entire development team, we have a first-class SCM/CI service that is easy to start using and supporting a quality first mindset.
    attribution: Brendan Mannix
    attribution_title: Vice President, Engineering
    content:
      - |
        Within the first six months of using GitLab, EAB had 888 projects across 80 groups including subgroups as well with 214 users. The company had 18,000 builds,
        which comes down to almost half a million individual jobs.


