---
layout: handbook-page-toc
title: Zendesk Schedules
description: Zendesk schedules are like schedules in most other things: windows of
time. We use these to determine business hours and various regional working
hours.
---

# Zendesk Schedules

Schedules arein Zendesk are like schedules in most other things: windows of
time. We use these to determine business hours and various regional working
hours.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Schedules

### [Business Hours](https://gitlab.zendesk.com/agent/admin/schedules/91387)

* Timezone: GMT -07:00 Pacific Time (US & Canada)
* Sunday: 1500-2400
* Monday: 0000-2400
* Tuesday: 0000-2400
* Wednesday: 0000-2400
* Thursday: 0000-2400
* Friday: 0000-1900
* Saturday: Closed

### [EMEA](https://gitlab.zendesk.com/agent/admin/schedules/360000029879)

* Timezone: GMT +02:00 Amsterdam
* Sunday: Closed
* Monday: 0800-1800
* Tuesday: 0800-1800
* Wednesday: 0800-1800
* Thursday: 0800-1800
* Friday: 0800-1800
* Saturday: Closed

### [AMER](https://gitlab.zendesk.com/agent/admin/schedules/360000029899)

* Timezone: GMT -07:00 Pacific Time (US & Canada)
* Sunday: Closed
* Monday: 0500-1700
* Tuesday: 0500-1700
* Wednesday: 0500-1700
* Thursday: 0500-1700
* Friday: 0500-1700
* Saturday: Closed

### [APAC](https://gitlab.zendesk.com/agent/admin/schedules/360000029919)

* Timezone: GMT +10:00 Brisbane
* Sunday: Closed
* Monday: 0900-2200
* Tuesday: 0900-2200
* Wednesday: 0900-2200
* Thursday: 0900-2200
* Friday: 0900-2200
* Saturday: Closed

### [Low Priority Tickets](https://gitlab.zendesk.com/agent/admin/schedules/360000044539)

* Timezone: GMT -07:00 Pacific Time (US & Canada)
* Sunday: 1500-2400
* Monday: 0000-2400
* Tuesday: 0000-2400
* Wednesday: 0000-2400
* Thursday: 0000-2400
* Friday: 0000-1900
* Saturday: Closed

## Holidays

All schedules should utilize the same holidays. The ones we currently put into Zendesk are:

* Easter (this is a "moveable feast" holiday, so it changes each year)
* Christmas (December 25th)
* New Years Day (January 1st)

On those day, the schedules "stop", which results in the SLA clock not moving. This allows for Support to actually have those holidays "off".
