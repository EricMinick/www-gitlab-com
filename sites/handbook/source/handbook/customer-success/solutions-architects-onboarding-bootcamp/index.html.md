---
layout: markdown_page
title: "Solutions Architect Onboarding"
---

This page has been deprecated and added to the [People Ops onboarding issue template](https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md).
