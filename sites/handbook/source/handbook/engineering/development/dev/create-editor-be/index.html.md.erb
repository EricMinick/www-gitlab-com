---
layout: handbook-page-toc
title: "Create:Editor Backend Engineering Team"
description: "The Create:Editor BE team is responsible for all backend aspects of the product categories that fall under the Editor group of the Create stage of the DevOps lifecycle"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Create:Editor Backend Engineering (BE) Team is responsible for all backend aspects of the product categories that fall under the [Editor group][group] of the [Create stage][stage] of the [DevOps lifecycle][lifecycle].

[group]: /handbook/product/product-categories/#editor-group
[stage]: /handbook/product/product-categories/#create-stage
[lifecycle]: /handbook/product/product-categories/#devops-stages
[work]: /handbook/engineering/development/dev/create-editor-be/work
[onboarding]: /handbook/engineering/development/dev/create-editor-be/onboarding
[communication]: /handbook/engineering/development/dev/create-editor-be/communication
[career]: /handbook/engineering/development/dev/create-editor-be/career-development
[blog]: /blog/2018/06/15/introducing-gitlab-s-integrated-development-environment/
[team]: https://www.youtube.com/watch?v=axF0qPqBEl0
[webIDE]: https://docs.gitlab.com/ee/user/project/web_ide/
[snippets_docs]: https://docs.gitlab.com/ee/user/snippets.html
[snippets_vision]: https://about.gitlab.com/direction/create/#snippets
[web_ide_vision]: https://about.gitlab.com/direction/create/#web-ide
[web_ide_docs]: https://docs.gitlab.com/ee/user/project/web_ide/index.html
[live_coding_vision]: https://about.gitlab.com/direction/create/#live-coding
[general_onboarding]: https://gitlab.com/gitlab-com/people-group/employment-templates-2/blob/master/.gitlab/issue_templates/onboarding.md#all-gitlabbers
[general_communication]: /handbook/communication/
[maturity]: https://about.gitlab.com/direction/maturity/
[engineering workflow]: /handbook/engineering/workflow/
[#g_create_editor]: https://gitlab.slack.com/archives/g_create_editor

## Team Members

The following people are permanent members of the Create:Editor BE Team:

<%= direct_team(manager_role: 'Backend Engineering Manager, Create:Knowledge & Create:Editor', role_regexp: /[,&] Create:Editor/) %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] (Create(?!:)|Create:Editor)/, direct_manager_role: 'Backend Engineering Manager, Create:Knowledge & Create:Editor') %>

## Product Categories

### Web IDE

A full-featured Integrated Development Environment (IDE) built into GitLab so you can start contributing on day one with no need to spend days getting all the right packages installed into your local dev environment.

- [Documentation][web_ide_docs]
- [Vision][web_ide_vision]
- [Meet the GitLab Web IDE][blog]
- [Update the Team Page with GitLab Web IDE][team]

### Snippets

Store and share bits of code and text with other users. This category is at the "minimal" level of [maturity][maturity].

- [Documentation][snippets_docs]
- [Vision][snippets_vision]

### Live Coding

Do pair programming directly in the Web IDE. This category is planned, but not yet available.

- [Vision][live_coding_vision]

## Hiring

This chart shows the progress we're making on hiring. Check out our
[jobs page](/jobs/) for current openings.

<%= hiring_chart(department: 'Create:Editor BE Team') %>

## Onboarding

There is a [general onboarding issue][general_onboarding] assigned to all GitLab team members by People Ops on your first day.

[Learn more][onboarding]

## Internship

The Create:Editor BE team has participated in the GitLab Engineering Internship program.  Interns are treated as equal members of the team and will follow all of the processes and procedures all team members follow.

[Learn more][internship]

## Communication

All GitLab Team members are expected to follow the General GitLab communications guidelines.

[Learn more][communication]

## Iteration

<%= partial("handbook/engineering/development/dev/create/_iteration.erb") %>

## Work

In general, we use the standard GitLab [engineering workflow]. To get in touch
with the Create:Editor BE team, it's best to create an issue in the relevant project and add the `~"group::editor"` and `~backend` labels, along with any other
appropriate labels. Then, feel free to ping the relevant Product Manager and/or
Engineering Manager, as listed above.

For more urgent items, feel free to use [#g_create_editor] on Slack.

[Learn more][work]

## Backlog Refinement

Every week the backend engineering team completes a backlog refinement process to review upcoming issues and provide weights so the issues can be planned and scheduled for upcoming milestones.

This process happens in three steps over the course of one week.

### Step 1: Identifying Issue for Refinement

Every Friday the engineering manager will identify issues that need to be refined the following week. On average we will try to refine 3-6 issues per week. When picking issues to refine, here are some places to look:

- [Issues that have been recently updated but have no weight](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aeditor&weight=None)
- [Issues that are scheduled for the next milestone with no weight](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aeditor&weight=None&milestone_title=13.5)
- [Backend Performance issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aeditor&weight=None&label_name[]=backend&label_name[]=performance)
- [Backend bugs](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aeditor&weight=None&label_name[]=backend&label_name[]=bug)
- [Application Limits](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aeditor&weight=None&label_name[]=backend&label_name[]=Application%20Limits)
- [All backend issues without weight](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aeditor&weight=None&label_name[]=backend)

Once identified, the engineering manager will apply the “Backlog Refinement::Editor” and “workflow::planning breakdown” label, which will indicate the issues are ready for refinement.

### Step 2: Refining Issues

Early in the next week, each backend engineer on the team will look at the list of issues selected for backlog refinement:

[Current backlog refinement issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Backlog%20Refinement%3A%3AEditor)

For each issue, each team member will review the issue and provide the following information:

- Estimated weight
- Documentation that needs to be updated
- Testing that needs to take place
- Any security concerns
- How you would use iteration to break down the issue into MRs

### Step 3: Finalizing Refinement

On the following Friday, after engineers have had a chance to provide input, the engineering manager will use the input to update the following information:

- Apply a final weight. This could be the average of weights provided by the engineers, but the final decision is up to the engineering manager
- Update the description of the issue as needed to add information the came up during the refinement process
- Inform stable counterparts if there are any testing or security concerns
- Remove the “Backlog Refinement::Editor” label and change the “workflow::planning breakdown” label to be “workflow::scheduling”

For any issues that were not discussed and given a weight, the engineering manager will work with the engineers to see if we need to get more information from PM or UX.

<!---
 ### Capacity planning

<%= partial("handbook/engineering/development/dev/create/capacity_planning.erb") %>

### What to work on

<%= partial("handbook/engineering/development/dev/create/what_to_work_on.erb", locals: { group: "Editor", slack_channel: 'g_create_editor' }) %>

[issue board]: https://gitlab.com/groups/gitlab-org/-/boards/412126?label_name[]=group::editor&label_name[]=backend
[assignment board]: https://gitlab.com/groups/gitlab-org/-/boards/1143526

### Retrospectives

<%= partial("handbook/engineering/development/dev/create/retrospectives.erb", locals: { group: "Editor", group_slug: 'editor' }) %>
--->

## Career Development

<%= partial("handbook/engineering/development/dev/create/career_development.erb", locals: { group: "Editor" }) %>

[Learn more][career]

<%= partial("handbook/engineering/development/dev/create/editor_knowledge_backup_shared.erb") %>
