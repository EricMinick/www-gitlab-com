---
layout: handbook-page-toc
title: "Critical Systems"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# What is a critical system?

Traditionally, a critical system has been defined as any system whose `failure` could threaten human life, the system's environment, or the existence of the organization that operates the system. At GitLab, a tiered approach has been taken to identify critical systems and **all** production systems in the environment are ranked by tiers. Additionally, ranking systems by tiers provides the ability to help determine the prioritization of work based on the tier assigned to a system.

## Critical System Tiers

All systems at GitLab are subject to a critical system tier. Tiering our systems allows us to effectively prioritize work based on a specific tier that a system belongs to. Each system is tiered based on the definitions in the table below.

|  **Tier Level** | **Definition** | **Examples of Associated Work** |
| ----- | ----- | ----- |
|  **Tier 1** | The system is in-scope for an external security compliance audit and/or is considered a [P1 system](/handbook/business-ops/gitlab-business-continuity-plan/#data-continuity-system). [Note that systems only in-scope for SOX are not included in this tier](#why-sox-compliance-is-excluded-from-tier-1). | Security Compliance owns Quarterly Access Reviews for these systems and will prioritize control testing activities for these systems first. Additionally, security impacting events and associated work will be prioritized for these systems. Vendor security reviews will always be performed at least annually for Tier 1 systems. |
|  **Tier 2** | The system is in-scope for SOX compliance and/or is considered a [P2 system](/handbook/business-ops/gitlab-business-continuity-plan/#data-continuity-system) | Quarterly Access Reviews are performed by the respective Business Owner and control testing is prioritized following Tier 1 systems. Vendor security reviews will be performed at minimum over specific systems impacting Financial Reporting. |
|  **Tier 3** | The system is not in-scope for any compliance requirements, but stores and/or processes RED or ORANGE data and/or is considered a [P3 system](/handbook/business-ops/gitlab-business-continuity-plan/#data-continuity-system) | Quarterly Access reviews are performed by the respective System Owner and control testing is prioritized after Tier 2 systems, as applicable. |
| **Tier 4** | All remaining systems not subject to Tiers 1-3 and/or [P4 system](/handbook/business-ops/gitlab-business-continuity-plan/#data-continuity-system) | These systems will be reviewed as part of the [Security Operational Risk Management (STORM)](/handbook/engineering/security/security-assurance/field-security/risk-management.html) Annual Risk Assessment and adjustments to system tiering will be made accordingly based on the annual assessment. |

`**Important Note:** While Security will prioritize work for Tier 1 systems, note that at GitLab, our highest priority is ensuring that our core service offerings, GitLab SaaS and Self-Managed solutions, are secure for our customers. As such, any security related work impacting these offerings will always be prioritized above all other systems, regardless of a system’s tier.`

### Why SOX Compliance is excluded from Tier 1

The critical system identification methodology is a tool used by the Security Organization to understand areas of focus and work prioritization due to impacts to security. SOX compliance is not included as part of the Tier 1 considerations because it is a compliance requirement driven by the business. Security compliance requirements considered for Tier 1 include the specific compliance audits that directly impact GitLab Security, such as SOC 2. 

## Business Continuity: P1 - P4 Systems

As part of the tiering methodology, GitLab has also considered the implications/impact of systems that are mission critical and support the ongoing day-to-day operational needs to sustain the business. The specific definitions for P1, P2, P3, and P4 systems are maintained by the [Business Operations](https://about.gitlab.com/handbook/business-ops/) team. Specific details and defintions can be found on the [Business Continuity Handbook](/handbook/business-ops/gitlab-business-continuity-plan/#data-continuity-system) page under the `Data Continuity System` section.

## Critical System Data Management Standards

Explicit data management standards are not considered as part of each critical system tier because these standards are established based on the assigned [Data Classification Label](/handbook/engineering/security/data-classification-standard.html#data-classification-standards) that is assigned to the system. GitLab utilizes the terms RED, ORANGE, YELLOW, and GREEN to describe and classify the type of data stored within systems. Based on this classification, there are **specific requirements** that are enforced. Additional details of the required **Security and Privacy** controls that each data classification label is subject to is documented on the [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html#security-and-privacy-controls) handbook page.

## Current listing of critical systems

As of today, GitLab's Critical System inventory is maintained in [this Google Sheet](https://docs.google.com/spreadsheets/d/1lbynw7uaFhv-uPJ1QETFmxN-SmObiEGkwUZIzqIy9uA/edit#gid=0). Eventually, this information will be included as part of the Tech Stack document that is used internally by GitLab Team Members. <!--TBD - Need to sync with Karlia on this to make sure we can deploy and include the critical system tier alongside updating the tech stack document.-->The [Tech Stack Documentation](https://about.gitlab.com/handbook/business-ops/tech-stack-applications/#tech-stack-documentation) is maintained by the [Business Operations](https://about.gitlab.com/handbook/business-ops/) team.

## When are critical systems identified

A critical system risk assessment is performed on an annual cadence in alignment with the [STORM annual risk assessment process](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/risk-management.html) to validate existing systems in GitLab’s environment and make adjustments to system tiers as applicable. Additionally, as part of the procurement process, as new systems are procured and added to the [Tech Stack](https://about.gitlab.com/handbook/business-ops/tech-stack-applications/#tech-stack-documentation), a critical system tier is applied.
