---
layout: handbook-page-toc
title: Learning & Development
---

# EdCast LXP Bootcamp

## Training Structure

### HPA Training Site

EdCast uses their platform to organize and deliver training content to members of the core team in a learning platform called HPA. If new members are onboarded to GitLab and need to access training materials in HPA, they should work with the core team and EdCast account manager to be authenticated.

Core team members can access the HPA learning materials at [https://hpa.edcast.com](https://hpa.edcast.com/).

All bootcamp content from the HPA is also accessible to GitLab team members only via Google Drive. We've listed each training below with a brief explanation of the content covered in each training.

### Training Session Structure

The GitLab team meets with the EdCast team frequently to train and implement the system. The following meeting formats are used to collaborate:

- Weekly Standup Meetings: Hosted one time per week by EdCast, these meetings are meant to be a short, 30 mintute check in and update on progress made and questions to address
- Weekly Implementation Status Meeting: Hosted one time per week by EdCast, these meetings are meant to review blockers, address big questions, and determine next steps in implementation
- Daily GitLab Huddle: Hosted one time per day by EdCast, these meetings are meant to be working Q&A sessions with the EdCast team and are optional.
- Marketplace Meetings: The Professional Services team meets with EdCast to train on and implement the EdCast Marketplace features for external audiences
- Additional Meetings: Individuals or small groups with specific use cases may host additional trainings and meetings with EdCast

If you would like to be added to any of the implementation meetings, please reach out to the core team in the [#proj-lxp-edcast Slack channel](https://app.slack.com/client/T02592416/C010VVAT48Y).

### Team Communication

The EdCast implementation team uses the [#proj-lxp-edcast Slack channel](https://app.slack.com/client/T02592416/C010VVAT48Y) to collaborate. The [Bootcamp Planning Issue](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/655) was used to define and organizd bootcamp sessions.

## Bootcamp Training Materials

### GitLab Bootcamp Directory

In addition to HPA and the bootcamp Google Drive, the [GitLab Bootcamp Topic Overview](https://docs.google.com/spreadsheets/d/1nSRNQljFvxOaiADcG97AFYrAj_6b5vcXI0O9vDDF2TM/edit#gid=3560912) serves as a resource to organize training recordings and can be viewed by GitLab team members.

### Administrator

#### Summary

GitLab team members using EdCast in the following capacity should complete the Administration bootcamp materials listed below:

- Onboarding learners, managing learner accounts
- Content release management
- Customization of the front end UI
- Adjustment of user settings and access
- Platform integrations with third party tools

#### Key Topics and Training Recordings

| Key Topic and Recording Link | Training Summary |
| ----- | ----- |
| [Admin Foundations Google Folder](https://drive.google.com/drive/folders/1KCJjozx0U_iA3-Tc_t2ojXeCveWn6QHk) | Main storage folder |
| [Understanding the difference between a Learning Experience Platform (LXP) and a Learning Management System (LMS)](https://drive.google.com/file/d/1rpxY8yU9hvleiTrXuFT_j7t8n31J-RCi/view) | Introduction to the LXP & who EdCast is |
| [Management of platform integrations](https://drive.google.com/file/d/1XkMejw3PYSkMYylSuEaYytHW9zg0TdQk/view) | Integrations with systems or content providers |
| [Front end UI Admin UI configuration](https://drive.google.com/file/d/1pRbuvkML1jPe3lzdCA-2rBI0AJ1Whfu_/view) | General difference between front-end & back-end UI |
| [Release Management](https://drive.google.com/file/d/1_ey-iJcx8hDNTBWlkMcOS1MP3PrDc3ZK/view) | Management of enabled features and functionality |
| [Front End and User Provisioning](https://hpa.edcast.com/insights/admin-bootcamp-10152020-mp4) | Branding, customization, and user provisioning|
| [User Management](https://hpa.edcast.com/insights/gitlab-admin) | User authentication, onboarding, roles and RBAC |
| [EdCast Groups](https://hpa.edcast.com/insights/gitlab-admin-bootcamp) | Understanding how to create and use learner groups |
| [Governance Features](https://hpa.edcast.com/insights/gitlab-admin-bootcamp) | Governance implementation and settings |
| [Notifications](https://hpa.edcast.com/insights/gitlab-bootcamp) | Management of notifications for users in EdCast |
| [Gamification Features](https://hpa.edcast.com/insights/gitlab-bootcamp-gamifications-mp4) | How to incorporate gamification |
| [Mobile Configuration Features](https://hpa.edcast.com/insights/mobile-lxp) | How to configure mobile setup |
| [User Analytics](https://www.youtube.com/watch?v=Mfa4gGotiKE) | Analytics settings and access |
| Admin Panel Content Connectors | TBD |
| Workflos | TBD |



### Curator

#### Summary

GitLab team members using EdCast in the following capacity should complete the Curator bootcamp materials listed below:

- Creation of content in the platform
- Curation of content
- Understanding types of learning content in Edcast - Smartcards, Pathways, Journeys, Channels
- Organizing and leveraging user groups
- Curation of the discover page

#### Key Topics and Training Recordings

| Key Topic and Recording Link | Training Summary |
| ----- | ----- |
| [Curator Foundations Google Drive](https://drive.google.com/drive/folders/1u-uTgLgWnbDfUbr7dbPH-3lV0TImSf_e) | Main storage folder |
| [Content Architecture](https://drive.google.com/file/d/1IeUap0u4LfF0rp9HJJ6IoAUpjjLmdMY6/view) | How EdCast content is organized |
| [Creation and Curation](https://drive.google.com/file/d/1hPloTJf0y31Wn-7pZcTiheo6gXQe56hB/view) | How to create and curate content |
| Content Assets | TBD |
| Smartcards | TBD |
| Pathways | TBD |
| Journeys | TBD |
| Channels | TBD |
| Groups | TBD |
| Discover Page | TBD |


### Marketplace Foundations

#### Summary

GitLab team members using EdCast in the following capacity should complete the Marketplace bootcamp materials listed below:

- Monetizing courses in EdCast to an external audience
- Creation and delivery of course certifications to be shared externally

#### Key Topics and Training Recordings

| Key Topic and Recording Link | Training Summary |
| ----- | ----- |
| [Marketplace Foundations Google Drive](https://drive.google.com/drive/folders/1nT1Su0__FISKHb6h845v1lmuUhwOcVr3) | Main storage folder |



## Additional Resources

### Governance

EdCast provides an outline of suggestions and best practices for role organization and governance in the platform. The GitLab team has used this document to plan and organize our core team and steering committee for project-wide decision making.

- [Governance Features](https://drive.google.com/file/d/1usxt8lO9nQk2yhUcHzKwSGaqYpJ-GP_N/view)

### Branding Resources

EdCast has provided the following guideslines and resources for branding in their system, and should be shared with our GitLab brand and design teams when requesting their support.

- [Color Customization Guide](https://drive.google.com/file/d/1BAk0YQzDUZFabtemxBi9h-JeAU__95Xd/view)
- [LXP Image Guidelines](https://drive.google.com/file/d/1VTsEGDd9_kLag8YuU9iSwWcEnACJl3CB/view)


