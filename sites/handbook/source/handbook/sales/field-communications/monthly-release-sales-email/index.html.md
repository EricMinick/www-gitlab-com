---
layout: handbook-page-toc
title: "GitLab Monthly Release Email to Sales"
description: "Following each GitLab release on the 22nd of each month, Field Communications sends out an email template that members of the sales organization can repurpose to send to their customers and prospects"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Following each GitLab release [on the 22nd of each month](/handbook/marketing/blog/release-posts/), Field Communications sends out an email template that members of the sales organization can repurpose to send to their customers and prospects. The purpose of this email is to make it easy for team members to quickly share the technical details of the latest release and create a meaningful touch-point with their customer or prospect. 

### Format 
The quick format includes: 
1. An overview of [GitLab's value drivers](/handbook/sales/command-of-the-message/)
1. 3-4 updates from GitLab, usually helpful content or big announcements 
1. All release details from the [GitLab Releases page.](https://about.gitlab.com/releases/)

### Process
1. Email is sent out by 9 am CT on the 22nd (or first business day) of each month
   1. Sent to sales-all email group 
   1. Reminder in #sales-all Slack channel
1. Email is also uploaded to [Outreach](/handbook/marketing/marketing-operations/outreach/) for any team members who prefer to use this tool to send to customers. 

### Helpful Resources
- [Adding templates via Outreach in Gmail](https://support.outreach.io/hc/en-us/articles/115003497854-Using-Outreach-Content-in-Gmail-Templates)
- [Composing a single email to send out in bulk](https://support.outreach.io/hc/en-us/articles/115004157734-Compose-a-Single-Email-Bulk-Compose)

Questions? Please reach out to #field-enablement-team
