---
layout: markdown_page
title: "Disaster Recovery Working Group"
description: "The charter of this working group is to "
canonical_path: "/company/team/structure/working-groups/disaster-recovery/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value            |
|-----------------|------------------|
| Date Created    | November 11, 2020 |
| End Date        | TBD              |
| Slack           | [#wg_disaster-recovery](https://gitlab.slack.com/archives/C01D6Q0DHAL) (only accessible from within the company) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1FOvwdwV7ncxTurctPFYYRSNLL1qFqCkSifdY_MiL5DY/edit) (only accessible from within the company) |
| Issue Board     | TBD             |

### Charter

This working group will determine what is needed to leverage GitLab Geo as a mechanism for building reliable and predictable disaster recovery into GitLab.com production. 

### Scope and Definitions 

In the context of this working group,

1. **Recovery Point Objective (RPO)** : targeted duration of time in which data might be lost due to a major incident.
1. **Recovery Time Objective (RTO)** : targeted duration of time and service level within which a business process must be restored
after a disaster to avoid unacceptable consequences of a break in business continuity.

### Sequence Order Of Deliverables

1. Plan and execute a test of a staging failover leveraging GitLab Geo by 2020-12-15
1. An evaluation of that failover in the form of a gap analysis of what would be needed to provide the necessary failover functionality for GitLab.com production
1. Roadmap of how and when gaps identified will be addressed 
1. Successive Geo failover in Staging which result in a successful full failover of Staging  
1. A design of how GitLab Geo would be used in production in the form of a blueprint and readiness review

## Roles and Responsibilities

| Working Group Role                       | Person                           | Title                                                           |
|------------------------------------------|----------------------------------|-----------------------------------------------------------------|
| Executive Stakeholder                    | Steve Loyd                       | VP of Infrastructure                                            |
| Facilitator                              | Brent Newton (Interim)           | Director of Infrastructure, Reliability                         |
| DRI                                      | Brent Newton                     | Director of Infrastructure, Reliability                         |
| Functional Lead                          | Andrew Thomas                    | Principal Product Manager, Enablement                           |
| Functional Lead                          | Fabian Zimmer                    | Senior Product Manager, Geo                                     |
| Functional Lead                          | Marin Jankovski                  | Sr Engineering Manager, Infrastructure, Delivery & Scalability  |
| Member                                   | Chun Du                          | Director of Engineering, Enablement                             |
| Member                                   | Nick Nguyen                         | Backend Engineering Manager, Geo                             |

## Related Links

- [Disaster Recovery at GitLab](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/library/disaster-recovery/index.md)
- [GitLab.com Disaster Recovery Proposal](/handbook/engineering/infrastructure/product-management/proposals/disaster-recovery/index.html)
