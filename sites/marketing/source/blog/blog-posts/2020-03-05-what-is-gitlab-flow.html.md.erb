---
title: "The problem with Git flow"
author: Suri Patel
author_gitlab: suripatel
author_twitter: gitlab
categories: open source
image_title: '/images/blogimages/whatisgitlabflow.jpg'
description: "Learn why Git flow complicates the lifecycle and discover an alternative to streamline development."
tags: git, workflow, code review
twitter_text: "Learn about GitLab Flow: The solution to streamlined software development"
featured: yes
postType: content marketing
merch_banner: merch_three
---
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "BlogPosting",
    "mainEntityOfPage": {
      "@type": "WebPage",
      "@id": "https://about.gitlab.com/blog/2020/03/05/what-is-gitlab-flow/"
    },
    "headline": "The problem with Git flow",
    "description": "Learn why Git flow complicates the lifecycle and discover an alternative to streamline development.",
    "image": "https://about.gitlab.com/images/blogimages/whatisgitlabflow.jpg",  
    "author": {
      "@type": "Organization",
      "name": "GitLab"
    },  
    "publisher": {
      "@type": "Organization",
      "name": "",
      "logo": {
        "@type": "ImageObject",
        "url": ""
      }
    },
    "datePublished": "2020-03-05"
  }
  </script>

Sometimes, you can have too much of a good thing. That’s certainly true with [Git flow](https://nvie.com/posts/a-successful-git-branching-model/), a well-known software development workflow that offers several options but can bog down users.

We developed GitLab Flow as the solution to eliminate messy complexity and streamline the development process. [GitLab Flow](/blog/2014/09/29/gitlab-flow/) brings issue tracking to the Git workflow, simplifying the process and removing confusion.

## The problem with Git flow

To understand how GitLab Flow works, it’s helpful to start by looking at the problems it tries to solve. In Git flow, there are two main pain points, both of which involve unnecessary branch switching.

Git flow forces developers to use the `develop` branch rather than the `master`. Because most tools default to using the master, there’s a significant amount of branch switching involved. Another frustrating aspect is [hotfix](https://stackoverflow.com/questions/46729813/how-to-use-a-Gitflow-hotfix-branch) and `release` branches, which are overkill for most organizations and completely unnecessary in companies practicing continuous delivery.

That brings us to GitLab Flow, a workflow that keeps everything simple and inclusive.

## GitLab Flow: a simplified branching strategy

GitLab Flow combines feature-driven development and feature branches with issue tracking. GitLab Flow integrates the Git workflow with an issue tracking system, offering a simple, transparent, and effective way to work with Git.

GitLab Flow is a way to make the relationship between the code and the issue tracker more transparent. Each change to the codebase starts with an issue in the issue tracking system. When you’re done coding or want to discuss the code, you can open a merge request. When the code is ready, the reviewer will merge the branch into master, creating a merge commit that makes this event easily visible in the future. Using GitLab Flow, teams can deploy a new version of code by merging master into a `production` branch, enabling them to quickly identify what code is in production. In this workflow, commits only flow downstream, ensuring that everything is tested in all environments.

GitLab Flow prevents the overhead of releasing, tagging, and merging that accompanies Git flow.

GitLab Flow in a nutshell:
- All features and fixes first go to master
- Allows for `production` or `stable` branches
- Bug fixes/hot fix patches are cherry-picked from master

## Breaking down the 10 stages of software development

GitLab Flow is a way to move from the idea stage to production, all while keeping everyone informed and productive. We identified [10 key stages](/blog/2016/10/25/gitlab-workflow-an-overview/#stages-of-software-development) of the development process that must happen in order for software to get into production. GitLab Flow makes it easy to account for all of them, while continuing to provide full visibility into the development lifecycle.

Broadly speaking, GitLab Flow is broken down into three main areas: `feature` branch, `production` branch, and `release` branch.

A `feature` branch is where the serious development work occurs. A developer creates a feature or bug fix branch and does all the work there rather than on a master branch. Once the work is complete, the developer creates a merge request to merge the work into the master branch.

The `production` branch is essentially a monolith – a single long-running production `release` branch rather than individual branches. It’s possible to create a tag for each deployable version to keep track of those details easily.

The last piece, the `release` branch, is key if you release software to customers. With every new release, you’ll create a stable branch from master and decide on a tag. If you need to do a patch release, be sure to cherry-pick critical bug fixes first, and don’t commit them directly to the stable branch.

## Follow the rules

Want to get the most out of GitLab Flow? Our CEO [Sid Sijbrandij](/company/team/#sytses) came up with [11 rules teams should always follow to achieve maximum efficiency](/blog/2016/07/27/the-11-rules-of-gitlab-flow/). The blog is worth a read in its entirety, but here are a few rules are timely reminders of the importance of test, even in a CI environment:

* **Test all commits**: Don’t wait to test until everything has been merged into `master`. Test commits along the way to catch problems earlier in the process.
* **And run _all_ tests on all the commits**, even if you have to run tests in parallel.
* **Code reviews > merging into `master`.** Why wait? "Don’t test everything at the end of the week," Sid writes. "Do it on the spot, because you'll be more likely to catch things that could cause problems, and others will also be working to come up with solutions."

## Take a deep dive

Take a look at GitLab Flow in action! 🍿

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube-nocookie.com/embed/InKNIvky2KE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

<%= partial "includes/blog/blog-merch-sidebar-dynamic" %>

Cover image by [Fabio Bracht](https://unsplash.com/@bracht?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/photos/_z0DiiaIhB4)
{: .note}
